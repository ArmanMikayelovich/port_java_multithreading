package com.energizeglobal.internship.Port.model.platform;

public enum PlatformType {
    FOOD, TECH, CLOTHES
}
