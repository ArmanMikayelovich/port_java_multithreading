package com.energizeglobal.internship.Port.model.platform;

import com.energizeglobal.internship.Port.model.ProductType;
import com.energizeglobal.internship.Port.model.ship.Ship;

public interface Platform {


    void enterToPlatform(Ship ship);


    ProductType getServingProductType();

}
